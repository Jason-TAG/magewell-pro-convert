# Magewell Pro Convert - NDI to HDMI

This is a Q-SYS Plugin for the Magewell Pro Convert NDI to HDMI.

> Bug reports and feature requests should be sent to Jason Foord (jf@tag.com.au).

## How do I get set up?

See [Q-SYS Online Help File - Plugins](https://q-syshelp.qsc.com/#Schematic_Library/plugins.htm)


## Properties

*This Plugin has no configurable properties.*

## Controls

#### Error Status

Displays the current number of errors, if any.

> This control is in the top center of all pages.

#### Session Status

Displays the name of the user account that is currently logged in.

> This control is in the top right hand corner of all pages.

### Dashboard Page
![Dashboard Page](./Screenshots/NDI%20to%20HDMI%20Dashboard.JPG)

#### Device Name

Displays the device's name.

#### Serial Number

Displays the device's serial number.

#### HW Version

Displays the device's hardware version.

#### FW Version

Displays the device's firmware version.

#### Output

Displays the current output.

#### CPU

Displays the current CPU usage percentage.

#### Memory

Displays the current memory usage percentage.

#### Temparature

Displays the current CPU temparature.

#### Board Index

Displays the device's board index value.

#### Up Time

Displays the device's current 'up time'.

#### SD Card

Displays the current SD card state.

#### Source Name

Shows a list of discovered NDI sources.

> Select a source from the list to set it as the active source.

#### Gain Reset

Reset the gain to 0dB.

#### Gain (dB)

Set the gain value of the device.

### Setup Page
![Setup Page](./Screenshots/NDI%20to%20HDMI%20Setup.JPG)

#### ID

The user ID to log in with.

#### Password

The user password to log in with.

#### IP Address

The IP Address of the device.

#### Connect

Toggles the connection to the device.

#### Error Log

A current log of errors for the most recent result of each HTTP request.

> The last result for each request type will overwrite any previous errors. If the last result of a request was ***OK*** then the error will disappear from this list.